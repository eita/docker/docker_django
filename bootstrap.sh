#!/bin/bash

COMMAND_READ="read"

echo ""
export DJANGO_PROJECT_DIR="$(basename $(cd -))"
export DJANGO_PROJECT_NAME=$DJANGO_PROJECT_DIR
echo -n "Informe o nome do projeto Django ['$DJANGO_PROJECT_NAME']: ";
${COMMAND_READ};
if [[ ! -z ${REPLY} ]]; then
    export DJANGO_PROJECT_NAME=${REPLY}
fi

echo ""
DJANGO_PROJECT_TITLE=$(sed -r 's/(^|-|_)(\w)/\ \U\2/g' <<<"$DJANGO_PROJECT_NAME")
export PROJECT_TITLE="Plataforma$DJANGO_PROJECT_TITLE"
echo -n "Informe o título do projeto ['$PROJECT_TITLE']: ";
${COMMAND_READ};
if [[ ! -z ${REPLY} ]]; then
    export PROJECT_TITLE=${REPLY}
fi

echo ""
export PROJECT_GIT_URL="https://gitlab.com/eita/$DJANGO_PROJECT_DIR.git"
echo -n "Informe a URL do repositório git do projeto [$PROJECT_GIT_URL]: ";
${COMMAND_READ};
if [[ ! -z ${REPLY} ]]; then
    export PROJECT_GIT_URL=${REPLY}
fi

echo ""
export DJANGO_PROJECT_NAME_LOWERCASE=$(echo "${DJANGO_PROJECT_NAME}" | sed -e 's/\(.*\)/\L\1/')
export IMAGE_APP_PRODUCTION="eitacoop/$DJANGO_PROJECT_NAME_LOWERCASE:1.0-python-gunicorn"
echo -n "Informe a imagem docker de produção [$IMAGE_APP_PRODUCTION]: ";
${COMMAND_READ};
if [[ ! -z ${REPLY} ]]; then
    export IMAGE_APP_PRODUCTION=${REPLY}
fi

export DOCKER_DIRNAME='docker_django'

export DOLLAR='$'

echo ""
echo "Gerando instruções de instalação ..."
envsubst < docker-instructions.md.template > ../docker-instructions.md

echo ""
echo "Gerando comando 'docker-django' para ações docker no projeto Django ..."
envsubst < script/docker-django.sh.template > ../docker-django.sh

chmod +x ../docker-django.sh

if [[ -z $(ls ../$DJANGO_PROJECT_NAME 2> /dev/null) ]]; then
    echo ""
    echo -n "O projeto Django '$DJANGO_PROJECT_NAME' não foi encontrado. Deseja criá-lo? [s/n]: "
    ${COMMAND_READ};
    if [[ $REPLY =~ ^[Ss]$ ]]; then
        ./bootstrap-project/bootstrap-project.sh
    fi
fi

unset PROJECT_TITLE
unset PROJECT_GIT_URL
unset IMAGE_APP_PRODUCTION
unset DJANGO_PROJECT_NAME
unset DOCKER_DIRNAME
unset DOLLAR

# back to root directory
cd ..

echo ""
echo "Copiando arquivo .dockerignore ..."
cp docker_django/app/.dockerignore .

echo ""
echo "Copiando diretório de configurações .envs ..."
cp -r docker_django/.envs .

echo ""
echo "Checando .envs/.env.dev config ..."
echo ""
echo "Por favor, tecle ENTER para editar seu arquivo de configurações de desenvolvimento."
echo "Você deve atribuir o parâmetro SECRET_KEY caso não tenha feito isso."
echo "No caso de uma instalação em produção, por favor edite o arquivo .envs/.env.prod ou considere as demais configurações."
echo ""
echo "Obs: Assim que terminar a configuração e fechar o arquivo, o bootstrap script será continuado."
${COMMAND_READ};
EDITOR=$(echo $EDITOR)
if [[ -z $EDITOR ]]; then
    EDITOR=vim
fi
$(echo $EDITOR) .envs/.env.dev

echo ""
echo " --------------------------------------------------------------------------------------------------------- "
echo ""
echo "A instalação foi concluída com sucesso!";
echo ""
echo "Por favor, tecle ENTER para iniciar a aplicação.";
echo "O seguinte comando será executado na raiz do projeto:";
echo "./docker-django.sh compose up -d";
echo "Saiba mais pelo help: ./docker-django.sh help";
echo ""
echo "Tecle CTRL+C para sair";
echo ""
${COMMAND_READ};
./docker-django.sh compose up -d
