Check a [latest version](https://github.com/docker/compose/releases) of docker-compose.

* To upgrade docker-compose on Debian execute:
    ```sh
    sudo apt-get remove docker-compose
    DOCKER_COMPOSE_VERSION=v2.9.0 \
        bash -c 'sudo curl -L "https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" \
        -o /usr/local/bin/docker-compose'
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    ```
