# docker_django / .gitlab-ci

## Enable gitlab-ci

1. Copy *.gitlab-ci* to root: `cp docker_django/.gitlab-ci/.gitlab-ci.yml .`
1. Edit *./.gitlab-ci* variables
1. Configure Gitlab CI/CD Variables on CI/CD Settings / Variables:
    * Credentials to docker hub:
        * CI_REGISTRY_USER
        * CI_REGISTRY_PASSWORD
    * Credentials to server deploy:
        * CI_DEPLOY_USER
        * CI_DEPLOY_HOST
        * CI_DEPLOY_PORT
    * Path to root project on deploy server:
        * CI_DEPLOY_PROJECT_ROOT
        * CI_DEPLOY_PROJECT_USER
    * URL of room to your [matrix](https://matrix.org) comunication:
        * CI_NOTIFY_MATRIX_URL


1. Create runners:

    ```sh
    docker run -d \
    --name gitlab-runner \
    --restart always \
    -v /usr/local/share/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
    ```

    * [RUNNER_PYTHON_TASKS](#create-runner_python_tasks): simple runner python to execute commands;
    * [RUNNER_DEPLOY](#create-runner_deploy): runner shell based using ssh-key to deploy


1. Custom Sphinx docs scripts:

    By default this solution run on production branch *docker_django/.gitlab-ci/scripts/sphinx-docs.sh* script with basic prepare.

    To customize:

    * copy sphinx-docs.sh wherever you like;
    * set path on *SPHINX_DOCS_SCRIPT* of *.gitlab-ci.yml*


1. Custom deploy scripts:

    By default this solution run on production branch *docker_django/.gitlab-ci/scripts/deploy.sh* script with basic prepare.

    To customize:

    * copy deploy.sh wherever you like;
    * set path on *DEPLOY_SCRIPT* of *.gitlab-ci.yml*

## Create RUNNER_PYTHON_TASKS

Temos um container do GitLab Runner de pé, pronto para ser um executor de tarefas.

Precisaremos sincronizá-lo com o projeto, nosso versionador e Integração Contínua. Como o GitLab saberá que há um runner disponível para o projeto?

Entraremos no menu de configurações e "CI/CD", onde haverá uma área específica para runners. Então, vamos registrar nosso runner no GitLab.

Alguns runners estão disponibilizados na internet, por isso, provavelmente quando iniciamos a Pipeline sem especificar um runner, ela utiliza algum executor de tarefas da área de compartilhamento.

Agora especificaremos nosso próprio runner copiando a chave do passo 3. da opção "Set up a specific runner. Vamos entrar no terminal, escrever clear para limpá-lo. Depois acessaremos o container com docker exec -it gitlab-runner bash. A partir de então, qualquer comando que executarmos será dentro do GitLab Runner.

Para fazer conectar o executor com o GitLab, faremos o registro com gitlab-runner register. Quando digitarmos esse comando, serão feitas algumas perguntas, como as coordenadas de "gitlab-ci", ou seja, onde ele está.

Como estamos usando o projeto que está em nuvem, utilizaremos o endereço sugerido mesmo, https:gitlab.com/. Pressionaremos "Enter" e passaremos para o passo seguinte.

Será pedido o token para o executor. Esse token é a chave que copiamos na página do GitLab, e colaremos nesse momento. Precisaremos colocar uma descrição para ele, e colocaremos runner bytebank. Há a opção de colocar tags separadas por vírgula, mas não colocaremos nenhuma. Passaremos para o passo seguinte.

O executor será registrado e agora será perguntado qual é o tipo do executor. Há vários tipos de executor, uma máquina docker, uma que shell script, ou acessível por ssh, e assim por diante. Usaremos o tipo docker.

Na sequência, teremos que informar em qual imagem o executor se baseará, a que criamos no passo de build . Chamamos ela de jnlucas/minha-imagem: latest, e assim, colaremos a imagem e a versão no terminal.

Feito isso, veremos que nosso runner foi registrado com sucesso e podemos sair do terminal com exit. Com um docker ps podemos nos certificar que o serviço continua ativo apesar de termos saído.

Voltando ao GitLab, atualizaremos a página. Vamos expandir novamente o menu de runners e o nosso terá sido criado com o nome de "runner-bytebank", e ele estará pronto para uso.

Clicaremos no ícone do lápis, ao lado do runner, para editá-lo. Acrescentaremos uma tag à ele, que fará a "amarração" com o passo no "gitlab-ci". Vamos chamá-la de executor-tarefas e salvaremos.

Temos, então, uma tag "executor-tarefas" no "runner". Voltaremos a "Settings > CI/CD", expandiremos os runners mais uma vez e nosso executor estará criado criado, pronto para uso, de acordo com o status, e com uma tag.

## Create RUNNER_DEPLOY

Criaremos um executor um pouco mais leve, já que o atual é baseado em docker, sempre precisa fazer o pull de uma imagem, e isso pode demorar, porque depende da rede de internet para baixar a imagem. Como não precisamos mais disso, geraremos um novo runner.

Abriremos o terminal e vamos entrar no container do GitLab Runner. Com um docker ps conseguiremos ver os containers que estão rodando na máquina. Encontraremos o nome do gitlab-runner:latest. Escrevendo exec - it gitlab -runner bash entraremos no container.

Agora vamos registrar um novo executor de tarefas com gitlab-runner register. O primeiro parâmetro que precisaremos informar será a URL onde estará nosso GitLab, https://gitlab.com/. O próximo passo será a chave de segurança que copiaremos na página do GitLab no navegador, em "CI/CD > Runners > Use the following registration token during setup".

Colaremos esse token no terminal e nesse momento colocaremos uma descrição para o runner, como runner-deploy, e pressionaremos o "Enter". Na sequência serão pedidas as tags que podem ser definidas aqui, mas definiremos no GitLab, como foi feito no executor anterior. Pressionaremos "Enter" para pular essa etapa.

Será questionado o que o executor fará, e como ele executará uma tarefa mais leve, vamos criá-lo do tipo shell e o runner será registrado com sucesso.

Voltaremos à página do GitLab, vamos atualizá-la e o novo runner com nome runner-deploy aparecerá. Clicaremos para editá-lo e definir a tag. Vamos adicionar a tag "executor-deploy" e salvar.

Clicando em "Settings > CI/CD", vamos expandir novamente a área de runners e veremos o runner ali. Não será necessário se preocupar com o ícone de exclamação aparecendo na lateral do executor, pois como ele é novo, ainda estará em fase de sincronização

## About RUNNER

Até agora, damos commit e push e a Pipeline tem sido executada automaticamente. Mas quem é o executor, ou em que máquina o job é executado? È uma máquina disponibilizada dentro do GitLab, e nós muitas vezes não temos controle dos procedimentos, por exemplo, em que ambiente a imagem está sendo gerada.

Para os próximos passos, precisaremos de um controle maior, pois mais adiante faremos compartilhamento de chaves, enfim, subiremos outros serviços. Então, criaremos nosso próprio executor de tarefas, o que é chamado de "runner" no "CI/CD" do GitLab.

Toda vez que dermos um push, o executor que executará a Pipeline será o que nós definirmos.

Voltaremos ao terminal e faremos um pull da última versão do GitLab Runner com docker pull gitlab/gitlabrunner:latest. A imagem ficará pronta para uso na máquina. Subiremos um container, pois não fará sentido fazer uma instalação completa. Assim, só teremos o necessário para que o GitLab Runner funcione.

Estará o comando para subir o container na área de exercícios do nosso curso, pois é um comando um pouco extenso. Colaremos ele no terminal e faremos um docker run, passando o atributo -d, porque assim subimos o serviço mas não criamos uma dependência.

Chamaremos nosso container de gitlab runner. O atributo --restart indica que o serviço cair, ele suba automaticamente na sequência, garantindo que o runner sempre funcione. -v serão os volumes, pastas que queremos compartilhar com o container.

Compartilharemos duas pastas com o GitLab Runner. É importante se atentar a qual será a pasta compartilhada dependendo do sistema operacional da máquina utilizada. No Mac IOS, a pasta será a "Users/Shared".

Depois, compartilharemos ainda um volume -v /var/run/docker.sock, com nosso container também. Por último, a imagem que usaremos, e acabamos de baixar. Pressionaremos "Enter" e subiremos o container. Faremos um docker ps para conferir se o container está de pé e veremos que sim.

Então, teremos nosso GitLab Runner para fazer as configurações que precisarmos. Esse será nosso executor para as próximas tarefas.
