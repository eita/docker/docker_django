#!/bin/bash

mkdir -p docs
cp -r docs_src/* docs/
cp README.md docs/
cp docker-instructions.md docs/
# python manage.py graph_models main > docs/model_main.dot
# python manage.py graph_models relatorios > docs/model_relatorios.dot
sphinx-apidoc -f -o docs . *migrations manage.py
sphinx-build -b html docs public
