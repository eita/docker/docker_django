#!/bin/bash

# ssh -T $CI_DEPLOY_USER@$CI_DEPLOY_HOST -p $CI_DEPLOY_PORT <<EOL >/dev/null
ssh -T $CI_DEPLOY_USER@$CI_DEPLOY_HOST -p $CI_DEPLOY_PORT <<EOL
cd $CI_DEPLOY_PROJECT_ROOT
git pull origin $BRANCH_PRODUCTION
cd docker_django
git pull origin master
cd ..
./docker-django.sh container-engine login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
./docker-django.sh container-engine pull $IMAGE_APP_PRODUCTION
./docker-django.sh service restart
sleep 10
./docker-django.sh exec-without-tty ./manage.py collectstatic --noinput
./docker-django.sh exec-without-tty ./manage.py migrate
./docker-django.sh exec-without-tty ./manage.py check --deploy
./docker-django.sh container-engine system prune -f
EOL

echo "Deploy executado!"
