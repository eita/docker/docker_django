from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

PROJECT_NAME = os.environ.get('DJANGO_PROJECT_NAME')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'{ PROJECT_NAME }.settings')

app = Celery(f'{ PROJECT_NAME }', backend='rpc://', broker='pyamqp://')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
