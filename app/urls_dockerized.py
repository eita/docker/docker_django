import os
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

exec(f"from { os.environ.get('DJANGO_PROJECT_NAME') }.urls import *")

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls, namespace='django-debug-toobar')),
    ]

# Defender - https://github.com/jazzband/django-defender
if settings.DEFENDER_ENABLE_ADMIN_DJANGO:
    urlpatterns = [
        path('admin/defender/', include('defender.urls')),
    ] + urlpatterns

urlpatterns += staticfiles_urlpatterns()
if settings.LOCAL:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
