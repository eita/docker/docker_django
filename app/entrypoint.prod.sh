#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z $SQL_HOST $SQL_PORT; do
    sleep 0.1
done

echo "PostgreSQL started"

if [ -z $DJANGO_SETTINGS_MODULE ]; then
    export DJANGO_SETTINGS_MODULE=docker_django.app.settings_dockerized; 
fi

# python manage.py flush --no-input
# python manage.py migrate
# python manage.py collectstatic

exec "$@"
