import os

SECRET_KEY = 'secret_key'

exec(f"from { os.environ.get('DJANGO_PROJECT_NAME') }.settings import *")

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY", "")

ROOT_URLCONF = os.environ.get("ROOT_URLCONF", "docker_django.app.urls_dockerized")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = bool(eval(os.environ.get("DEBUG", 'False')))
LOCAL = bool(eval(os.environ.get("LOCAL", 'False')))

ALLOWED_HOSTS = list(eval(os.environ.get("ALLOWED_HOSTS", "['*']")))
INTERNAL_IPS = list(eval(os.environ.get("INTERNAL_IPS", "['127.0.0.1', 'localhost']")))

FRONTEND_PORT = os.environ.get("FRONTEND_PORT", "3000")
CORS_ALLOWED_ORIGINS    = list(eval(
    os.environ.get("CORS_ALLOWED_ORIGINS",
                   f"['http://localhost:{ FRONTEND_PORT }', 'http://frontend:{ FRONTEND_PORT }']")
))
CORS_ORIGIN_WHITELIST   = list(eval(
    os.environ.get("CORS_ORIGIN_WHITELIST",
                   f"['http://localhost:{ FRONTEND_PORT }', 'http://frontend:{ FRONTEND_PORT }']")
))

if LOCAL:
    SESSION_COOKIE_NAME = f"{ os.environ.get('DJANGO_PROJECT_NAME') }.local"
else:
    SESSION_COOKIE_NAME = f"{ os.environ.get('DJANGO_PROJECT_NAME') }.production"

CSRF_COOKIE_SECURE = bool(eval(os.environ.get("CSRF_COOKIE_SECURE", 'False')))

try:
    import django_extensions
    if 'django_extensions' not in INSTALLED_APPS:
        INSTALLED_APPS += [
                'django_extensions',
                ]
except:
    pass

if LOCAL and DEBUG:
    import socket
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS = [ip[: ip.rfind(".")] + ".1" for ip in ips] + INTERNAL_IPS

    if not 'debug_toolbar' in INSTALLED_APPS:
        INSTALLED_APPS += [
            'debug_toolbar',
        ]

    if not 'debug_toolbar.middleware.DebugToolbarMiddleware' in MIDDLEWARE:
        MIDDLEWARE += [
            'debug_toolbar.middleware.DebugToolbarMiddleware',
        ]

# Defender - https://github.com/jazzband/django-defender
DEFENDER_ENABLE_ADMIN_DJANGO = bool(eval(os.environ.get("DEFENDER_ENABLE_ADMIN_DJANGO", "False")))
try:
    import defender

    if not 'defender' in INSTALLED_APPS:
        INSTALLED_APPS += [
            'defender',
        ]

    if not 'defender.middleware.FailedLoginMiddleware' in MIDDLEWARE:
        MIDDLEWARE += [
            'defender.middleware.FailedLoginMiddleware',
        ]

    DEFENDER_LOGIN_FAILURE_LIMIT        = int(os.environ.get("DEFENDER_LOGIN_FAILURE_LIMIT", "5"))
    DEFENDER_REDIS_URL                  = os.environ.get("DEFENDER_REDIS_URL", "redis://redis:6379/0")
    DEFENDER_REDIS_PASSWORD_QUOTE       = bool(eval(os.environ.get("DEFENDER_REDIS_PASSWORD_QUOTE", "False")))
    DEFENDER_REDIS_NAME                 = os.environ.get("DEFENDER_REDIS_PASSWORD_QUOTE", None)
    DEFENDER_BEHIND_REVERSE_PROXY       = bool(eval(os.environ.get("DEFENDER_BEHIND_REVERSE_PROXY", "True")))
    DEFENDER_DISABLE_IP_LOCKOUT         = bool(eval(os.environ.get("DEFENDER_DISABLE_IP_LOCKOUT", "False")))
    DEFENDER_DISABLE_USERNAME_LOCKOUT   = bool(eval(os.environ.get("DEFENDER_DISABLE_USERNAME_LOCKOUT", "False")))
    _DEFENDER_REVERSE_PROXY_HEADER      = os.environ.get("DEFENDER_REVERSE_PROXY_HEADER", None)
    if _DEFENDER_REVERSE_PROXY_HEADER:
        DEFENDER_REVERSE_PROXY_HEADER   = _DEFENDER_REVERSE_PROXY_HEADER
except:
    pass

# check --deploy
SECURE_SSL_REDIRECT     = bool(eval(os.environ.get("SECURE_SSL_REDIRECT", "False")))
SESSION_COOKIE_SECURE   = bool(eval(os.environ.get("SESSION_COOKIE_SECURE", "False")))
CSRF_COOKIE_SECURE      = bool(eval(os.environ.get("CSRF_COOKIE_SECURE", "False")))
SECURE_HSTS_SECONDS     = int(os.environ.get("SECURE_HSTS_SECONDS", "0"))
_SECURE_PROXY_SSL_HEADER = os.environ.get("SECURE_PROXY_SSL_HEADER", None)
if _SECURE_PROXY_SSL_HEADER:
    SECURE_PROXY_SSL_HEADER = tuple(eval(_SECURE_PROXY_SSL_HEADER))
SECURE_HSTS_INCLUDE_SUBDOMAINS = bool(eval(os.environ.get("SECURE_HSTS_INCLUDE_SUBDOMAINS", "False")))
SECURE_HSTS_PRELOAD = bool(eval(os.environ.get("SECURE_HSTS_PRELOAD", "False")))

# Database
DATABASES = {
    "default": {
        "ENGINE":  os.environ.get("SQL_ENGINE", "django.db.backends.postgresql"),
        "NAME": os.environ.get("SQL_DATABASE", "app_db"),
        "USER": os.environ.get("SQL_USER", "app_dbuser"),
        "PASSWORD": os.environ.get("SQL_PASSWORD", "password_db"),
        "HOST": os.environ.get("SQL_HOST", "db"),
        "PORT": os.environ.get("SQL_PORT", "5432"),
    },
}

# Data warehouse
COMPOSE_DATA_WAREHOUSE = bool(eval(os.environ.get("COMPOSE_DATA_WAREHOUSE", "False")))
if COMPOSE_DATA_WAREHOUSE:
    DATABASES["dw"] = {
        "ENGINE":  os.environ.get("DW_ENGINE", "django.db.backends.postgresql"),
        "NAME": os.environ.get("DW_DATABASE", "app_db"),
        "USER": os.environ.get("DW_USER", "app_dbuser"),
        "PASSWORD": os.environ.get("DW_PASSWORD", "password_db"),
        "HOST": os.environ.get("DW_HOST", "dw"),
        "PORT": os.environ.get("DW_PORT", "5432"),
    }


COMPOSE_REDIS = bool(eval(os.environ.get("COMPOSE_REDIS", "False")))
if COMPOSE_REDIS:
    try:
        import django_redis

        CACHES = {
            "default": {
                "BACKEND": "django_redis.cache.RedisCache",
                "LOCATION": "redis://redis:6379/1",
                "OPTIONS": {
                    "CLIENT_CLASS": "django_redis.client.DefaultClient",
                    # "PASSWORD": "mysecret"
                }
            }
        }

        CACHE_TTL = 20
        SESSION_ENGINE = "django.contrib.sessions.backends.cache"
        SESSION_CACHE_ALIAS = "default"
    except:
        pass

    CHANNEL_LAYERS = {
        'default' : {
            'BACKEND': 'channels_redis.core.RedisChannelLayer',
            'CONFIG' : {
                "hosts" : [('redis', 6379)], # You are supposed to use service name and not localhost
            },
        },
    }

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = os.environ.get("LANGUAGE_CODE", "pt-br")
TIME_ZONE = os.environ.get("TIME_ZONE", "America/Sao_Paulo")
USE_I18N = bool(eval(os.environ.get("USE_I18N", 'True')))
USE_L10N = bool(eval(os.environ.get("USE_L10N", 'True')))
USE_TZ = bool(eval(os.environ.get("USE_TZ", 'True')))


COMPOSE_ASYNC_TASKS = bool(eval(os.environ.get("COMPOSE_ASYNC_TASKS", 'False')))
if COMPOSE_ASYNC_TASKS:
    if not 'django_celery_results' in INSTALLED_APPS:
        INSTALLED_APPS += [
            'django_celery_results',
        ]

    CELERY_BROKER_URL = 'pyamqp://rabbitmq'

    # CELERY - RESULTS
    CELERY_RESULT_BACKEND = 'django-db'
    # CELERY_CACHE_BACKEND = 'django-cache'
    # CELERY_ENABLE_UTC = False
    # CELERY_TIMEZONE = 'America/Sao_Paulo'

    from datetime import timedelta

    CELERY_RESULTS_EXPIRES = {
        'report_update_task': timedelta(days=7),
        'report_caderno_update_task': timedelta(days=7),
        'set_municipios': timedelta(days=1),
        'CeleryHaystackSignalHandler': timedelta(days=1)
    }


COMPOSE_SEARCH_INDEX = bool(eval(os.environ.get("COMPOSE_SEARCH_INDEX", 'False')))
if COMPOSE_SEARCH_INDEX:
    if not 'haystack' in INSTALLED_APPS:
        INSTALLED_APPS += [
            'haystack',
        ]
    if not 'celery_haystack' in INSTALLED_APPS:
        INSTALLED_APPS += [
            'celery_haystack',
        ]

    HAYSTACK_SEARCH_RESULTS_PER_PAGE = 100
    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
            # 'ENGINE': 'haystack_solr_66.backends.solr_backend.SolrEngine66',
            # 'ENGINE': 'haystack_forked.backends.solr_backend.SolrEngine',
            # 'URL': 'http://127.0.0.1:8983/solr/tester',
            'URL': 'http://solr:8983/solr/tester',
            # 'ADMIN_URL': 'http://127.0.0.1:8983/solr/admin'
            'ADMIN_URL': 'http://solr:8983/solr/admin'
            # ...or for multicore...
            # 'URL': 'http://127.0.0.1:8983/solr/mysite',
        },
    }

    # HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
    # HAYSTACK_SIGNAL_PROCESSOR = 'main.signals.DataCPTSignalProcessor'
    # HAYSTACK_SIGNAL_PROCESSOR = 'celery_haystack.signals.CelerySignalProcessor'
    HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.BaseSignalProcessor'
    HAYSTACK_DEFAULT_OPERATOR = 'OR'


STATIC_URL = os.environ.get("STATIC_URL", "/static/")
STATIC_ROOT = os.path.join(
    BASE_DIR,
    os.environ.get("STATIC_ROOT", "static")
)

MEDIA_URL = os.environ.get("MEDIA_URL", '/media/')
MEDIA_ROOT = os.path.join(
    BASE_DIR,
    os.environ.get("MEDIA_ROOT", "media")
)

EMAIL_HOST          = os.environ.get("EMAIL_HOST", "")
EMAIL_HOST_USER     = os.environ.get("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD", "")
EMAIL_PORT          = int(os.environ.get("EMAIL_PORT", "465"))
EMAIL_USE_TLS       = bool(eval(os.environ.get("EMAIL_USE_TLS", "False")))
EMAIL_USE_SSL       = bool(eval(os.environ.get("EMAIL_USE_SSL", "False")))
SERVER_EMAIL        = os.environ.get("SERVER_EMAIL", "")
if not SERVER_EMAIL:
    SERVER_EMAIL    = os.environ.get("EMAIL_HOST_USER", "")


EMAIL_SUBJECT_PREFIX= os.environ.get("EMAIL_SUBJECT_PREFIX", "")
ACCOUNT_EMAIL_SUBJECT_PREFIX = os.environ.get("EMAIL_SUBJECT_PREFIX", "")
DEFAULT_FROM_EMAIL  = os.environ.get("DEFAULT_FROM_EMAIL", "")
ADMINS              = tuple(eval(os.environ.get("ADMINS", "(('Admin Name', 'admin@mail'),)")))
MANAGERS            = tuple(eval(os.environ.get("MANAGERS", "(('Manager Name', 'manager@mail'),)")))

# Show message in terminal instead send mail. Set only in dev env
if os.environ.get("EMAIL_BACKEND", ""):
    EMAIL_BACKEND = os.environ.get("EMAIL_BACKEND", "")

MAPBOX_ACCESS_TOKEN = os.environ.get("MAPBOX_ACCESS_TOKEN", "")

SITE_URL = os.environ.get("SITE_URL", "")

DEFAULT_AUTO_FIELD = os.environ.get("DEFAULT_AUTO_FIELD", "django.db.models.BigAutoField")


# TEMPLATE DEFAULT
if not TEMPLATES[0]['DIRS']:
    TEMPLATES[0]['DIRS'] = [
            os.path.join(BASE_DIR, 'templates/'),
            ]

CHROME_PATH = os.environ.get("CHROME_PATH", "")

STATIC_REPORT_URL = os.environ.get("STATIC_REPORT_URL", "http://localhost:8000/")

# LOG - LOCAL
# handler: console - level: info
# handler: file - level: warnning
# LOG - PROD
# handler: file - level: error
# handler: mail - level: error

LOG_ROOT = os.path.join(BASE_DIR, 'logs')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'file_debug': {
            'level': 'WARNING',
            'filters': ['require_debug_true'],
            'class': 'logging.FileHandler',
            'filename': f'{LOG_ROOT}/debug.log',
            'formatter': 'standard',
        },
        'file_info': {
            'level': 'INFO',
            'filters': ['require_debug_false'],
            'class': 'logging.FileHandler',
            'filename': f'{LOG_ROOT}/info.log',
            'formatter': 'standard',
        },
        'file_error': {
            'level': 'ERROR' ,
            'class': 'logging.FileHandler',
            'filename': f'{LOG_ROOT}/error.log',
            'formatter': 'standard',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'root': {
        'handlers': ['file_info'],
        'level': 'INFO',
    },
    'loggers': {
        'django': {
            'level': 'WARNING',
            'handlers': ['console',
                         'file_debug',
                         'file_error',
                         'mail_admins',
                         ],
            'propagate': True,
        },
    },
}
