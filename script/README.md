```sh
              ================================================================
                             Help to docker-django command line
              ================================================================

docker-django provide commands shortcut to basic operations of containers engine (podman or docker) into your Django project.

Usage:  ./docker-django.sh COMMAND [SUBCOMMANDS | OPTIONS]

Commands:
      compose       Shortcut to docker-compose related functionality.
                    Some options to consider:

        up          option to up compose containers. Consider use -d option to detach mode.
                    The PORT exposed to the web service will be assigned on the first run and
                    will be stored in the .envs/.env.dev file as the RUNSERVER_PORT parameter.

        down        option to down compose containers.
        ps          option to check status of compose containers.
        build       option to build python web app container. Consider use --no-cache option to force rebuild.
        help        check all options of docker-compose.

      attach        Attach local standard input, output, and error streams to python web app container.

      exec          Execute a command in web container.

      service       Manage docker stack services on production.
        start
        stop
        restart

      restore       Restore backup from aws s3 bucket. The settings for this command are
                    stored in .envs/.env.aws.restore, after the first run.
        db          option to restore database.

      help | -h     This help text.
```
