# Dockerizing Django

The docker_django aims to encapsulate a Django project in a container infrastructure.
This infrastructure is modular, extensible and configuration-based.

## Requirements

* Using Docker:
  * [Docker](https://docs.docker.com/engine/install/)
  * [Docker-Compose](https://docs.docker.com/compose/install/)
      * note about [compose upgrade](https://gitlab.com/eita/docker/docker_django/-/blob/master/docker-compose-upgrade.md)

* Using Podman:
  * [Podman](https://podman.io/getting-started/installation)
  * [Podman-Compose](https://github.com/containers/podman-compose#installation)
* Require *gettext-base* `sudo apt install gettext-base`
* Require *vim* or *$EDITOR* assigned `echo $EDITOR` or `sudo apt install vim`

## Bootstrap script

Running the `bootstrap` script will start it:
* Configuration of the infrastructure composition modules;
* Configuration of the application for development;
* Initialization of services.

## Bootstrap-project script

If a Django project is not found in the root directory, you have the option to create and configure a new project.

## Bootstrap a new Django project

1. Create a new project directory
    `mkdir new_django_project && cd new_django_project`

1. Start the versioning framework
    `git init`

1. Add docker_django as git submodule
    `git submodule add https://gitlab.com/eita/docker/docker_django.git`

1. Go to the docker_django directory
    `cd docker_django`

1. Execute bootstrap script
    `./bootstrap.sh`

1. Follow the step-by-step and at the end your project will be running.

1. Go back to the main directory and start interacting with the containers:
    ```sh
    cd ..
    ./docker-django.sh help
    ```

## Main commands for interacting with the container structure

* Check the status of your container composition
    `./docker-django.sh compose ps`

* Break down container structure
    `./docker-django.sh compose down -t0`

* Initialize the container infrastructure of the Django project
    `./docker-django.sh compose up -d`

* Rebuild container infrastructure
    `./docker-django.sh compose up -d --build --force-recreate`

* Exec any commands on Python container
    `./docker-django.sh exec ./manage.py migrate`, for example

* To check logs:
    * List services: `./docker-django.sh compose ps`
    * Check logs of service: `./docker-django.sh container-engine logs [Container ID or name]`

* To attach service:
    * Attach Python container: `./docker-django.sh attach`
    * Attach others container: `./docker-django.sh container-engine attach [Container ID or name]`
