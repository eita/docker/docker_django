#!/bin/bash

# sair do diretorio docker_dajngo
cd ..

echo ""
echo "Checando instalação ..."
if [[ ! -z $(ls $DJANGO_PROJECT_NAME 2> /dev/null) ]]; then
    echo ""
    echo "ERRO: O projeto $DJANGO_PROJECT_NAME parece já existir."
    echo "Para reinstalar remova-o e tente novamente."
    exit;
fi

if [[ $(podman --version 2> /dev/null) ]]; then
    CONTAINER_ENGINE_NAME=podman
elif [[ $(docker --version 2> /dev/null) ]]; then
    CONTAINER_ENGINE_NAME=docker
else
    CONTAINER_ENGINE_NAME=
fi

echo ""
echo "Utilizando $CONTAINER_ENGINE_NAME ..."

container-engine() {
    if [[ $(podman --version 2> /dev/null) ]]; then
        podman "$@"
    elif [[ $(docker --version 2> /dev/null) ]]; then
        docker "$@"
    else
        echo ""
        echo "No CONTAINER ENGINE was found on your operating system."
        echo "This program requires the installation of PODMAN or DOCKER to continue."
        echo ""
        exit;
    fi
}

echo ""
echo "Copiando requirement files ..."
cp -R docker_django/bootstrap-project/requirements* .

echo ""
echo "Criando diretórios: static; media e logs, com permissão de gravação ..."
mkdir logs 2>/dev/null;   touch logs/.empty;   chmod -R 777 logs
mkdir media 2>/dev/null;  touch media/.empty;  chmod -R 777 media
mkdir static 2>/dev/null; touch static/.empty; chmod -R 777 static

echo ""
echo "Copiando arquivo .gitignore ..."
cp ./docker_django/bootstrap-project/.gitignore.template ./.gitignore

echo ""
echo "Criando diretório de templates ..."
mkdir templates 2> /dev/null

# Configurando editor
EDITOR=$(echo $EDITOR)
if [[ -z $EDITOR ]]; then
    EDITOR=vim
fi

echo ""
echo "Caso você pretenda trabalhar com alguma biblioteca JavaScript para o frontend, como ReactJS, será necessário uma estrutura de diretórios."
echo -n "Deseja criar a estrutura de frontend agora? [s/n]: "
read
FRONTEND_FORCE_YARN=0
if [[ $REPLY =~ ^[Ss]$ ]]; then
    FRONTEND_FORCE_YARN=1

    echo ""
    echo "Criando diretório frontend ..."
    mkdir frontend 2> /dev/null
    touch frontend/yarn.lock 2> /dev/null
    mkdir frontend-node_modules
    mkdir frontend/public
    touch frontend/public/index.html
    mkdir frontend/src
    touch frontend/src/index.js

    echo ""
    echo "Criando arquivo de requerimentos do frontend package.json ..."
    cp ./docker_django/frontend/package.template.json frontend/package.json 2> /dev/null
    $(echo $EDITOR) frontend/package.json
fi

echo ""
echo " --------------------------------------------------------------------------------------------------------- "
echo ""
echo "Checando compose-addons config ..."
if [[ -z $(ls docker-compose-addons.cfg 2> /dev/null) ]]; then
    echo ""
    echo "ATENÇÂO: Arquivo de configurações do compose-addons não encontrado."
    echo ""
    echo "Você precisa dizer quais modulos de compose serão necessários para esta aplicação."
    echo "Para isso, comente ou descomente as linhas que iniciam com a flag -f"
    echo "Por favor, tecle ENTER para editar seu arquivo de configurações."
    echo ""
    echo "Obs: Assim que terminar a configuração e fechar o arquivo, o bootstrap script será continuado."
    echo ""
    echo -n "Tecle CTRL+C para sair";
    read
    cp docker_django/bootstrap-project/docker-compose-addons.cfg .
    $(echo $EDITOR) docker-compose-addons.cfg
fi

echo ""
echo "Checando container ..."
if [[ -z $(container-engine image ls | grep $DJANGO_PROJECT_NAME) ]]; then
    echo ""
    echo "Build container ..."
    echo ""
    ./docker-django.sh compose build --no-cache
else
    echo ""
    echo "Usando imagem existente ..."
fi

echo ""
echo "Criando projeto $DJANGO_PROJECT_NAME ..."
if [[ $CONTAINER_ENGINE_NAME == podman ]]; then
    container-engine run --rm -it -v .:/usr/src/app/ localhost/${DJANGO_PROJECT_NAME}_web django-admin startproject $DJANGO_PROJECT_NAME .
else
    container-engine run --rm -it -v $(pwd):/usr/src/app/ ${DJANGO_PROJECT_NAME}_web django-admin startproject $DJANGO_PROJECT_NAME .
fi

if [[ $FRONTEND_FORCE_YARN == 1 ]]; then
    container-engine run -it \
        -v $(pwd)/frontend:/app/frontend/ \
        -v $(pwd)/frontend-node_modules:/app/frontend/node_modules/ \
        ${DJANGO_PROJECT_NAME}_frontend:latest yarn
fi

echo ""
echo "==========================================================================="
echo ""
echo "Opcionalmente, configure o título do site admin."
echo "Para isso, adicione as linhas seguintes no arquivo urls.py ou admin.py:"
echo ""
echo "from django.contrib import admin"
echo ""
echo "admin.site.site_title  = '$PROJECT_TITLE'"
echo "admin.site.site_header = '$PROJECT_TITLE'"
echo "admin.site.index_title = '$PROJECT_TITLE'"
echo ""
echo "==========================================================================="
echo ""
echo -n "Pressione ENTER para continuar"
read

# add remote git repos $PROJECT_GIT_URL (check before)
